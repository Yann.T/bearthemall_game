﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
	public byte m_maxHealth = 100;
    [SerializeField]
    public byte m_health = 100;
    [SerializeField]
    protected float m_speed = 1;
    [SerializeField]
    protected float m_friction = (float)1.15;
    protected float m_damage = 0;

    protected Vector2 direction;

    /// <summary>
    /// Procedure which lets the characters move in space.
    /// </summary>
    public void Move()
    {
        direction = direction / m_friction;
        transform.Translate(direction * m_speed * Time.deltaTime);
    }

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        m_damage = 0;

        if (col.gameObject.tag == "explosion")
        {
            m_damage = (2 - (col.gameObject.transform.position - gameObject.transform.position).magnitude) * (-10);
            gameObject.GetComponent<Rigidbody2D>().AddForce((col.gameObject.transform.position - gameObject.transform.position) * -100);
            Debug.Log("Blasted off : " + m_damage);
        }
        else
        {
            switch (col.gameObject.tag)
            {
                case "bullet":
                    m_damage = 20;
                    break;
                case "pellet":
                    m_damage = 10;
                    break;
                case "grenade":
                    m_damage = 5;
                    break;
                case "rocket":
                    m_damage = 100;
                    break;
            }
        }

            if (m_health - m_damage > 0) m_health -= (byte)m_damage;
            else m_health = 0;
            m_damage = 0;
            //Debug.Log("Character got hit - life left > " + m_health);

    }

	protected virtual void Update()
    {
        Move();
    }
}